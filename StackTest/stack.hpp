#include <iostream>
#include <vector>
#include <forward_list>

// vector를 이용한 스택 구현
template<class T>
class Stack_Vector
{
private:
	std::vector<T> v;

public:
	Stack_Vector() {}
	// stack 기능 구현
	Stack_Vector& push(const T& val) { v.push_back(val); return *this; }
	Stack_Vector& pop()              { v.pop_back(); return *this; }
	const T& top()                   { return v.back(); }
	inline bool empty()              { return v.empty(); }
	inline std::size_t size()        { return v.size(); }
	auto begin()                     { return v.begin(); }
	auto end()                       { return v.end(); }
};

// forward_list를 이용한 스택 구현
template<class T>
class Stack_Forward
{
private:
	std::forward_list<T> l;
public:
	Stack_Forward() {}
	// stack 기능 구현
	Stack_Forward& push(const T& val) { l.push_front(val); return *this; }
	Stack_Forward& pop()              { l.pop_front(); return *this; }
	const T& top()                    { return l.front(); }
	inline bool empty()               { return l.empty(); }
	inline std::size_t size()         { return std::distance(l.begin(), l.end()); }
	auto begin()                      { return l.begin(); }
	auto end()                        { return l.end(); }
};