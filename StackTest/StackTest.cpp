﻿
#include "stack.hpp"
#include <iostream>
#include <stack>
#include <string>

// 올바른 괄호 검사
bool paren_check(const std::string& str)
{
    std::stack<char> stk;
    for (auto c : str)
    {
        if (c == '(' || c == '{' || c == '[')
        {
            stk.push(c);
        }
        else if (c == ')' || c == '}' || c == ']') // 닫는괄호가 나오면 쌍이 맞는지 체크
        {
            if (stk.empty())
                return false;

            if ((c == ')' && stk.top() == '(')
                || (c == '}' && stk.top() == '{')
                || (c == ']' && stk.top() == '['))
            {
                stk.pop();
            }
        }
    }
    return stk.empty();
}

int main()
{
    std::cout << paren_check("(){}[]") << std::endl;
    std::cout << paren_check("((((()))))") << std::endl;
    std::cout << paren_check("()([{}])") << std::endl;

    std::cout << paren_check("((({}))") << std::endl;
    std::cout << paren_check(")(") << std::endl;
    std::cout << paren_check("({)}") << std::endl;
}

/*
// 입력받은 문자열을 거꾸로 하여 반환하는 함수

std::string reverse(const std::string& str)
{
    std::stack<char> stk;
    for (auto c : str)
    {
        stk.push(c);
    }
    std::string result;

    while (!stk.empty())
    {
        result += stk.top();
        stk.pop();
    }
    return result;
}

// 입력받은 벡터 배열 순서 뒤집기
template<class T>
std::vector<T> reverseVector(std::vector<T> vec)
{
    // vec 값을 stack에 넣기
    std::stack<T> in;
    for (auto e : vec)
    {
        in.push(e);
    }
    // stack 값을 vec에 꺼내어 저장
    std::vector<T> res;
    while (!in.empty())
    {
        res.push_back(in.top());
        in.pop();
    }
    return res;
}

int main()
{
    // 문자열 뒤집기
    std::string str1 = "HELLO";
    std::cout << reverse(str1) << std::endl;

    std::string str2 = "ALGORITHM";
    std::cout << reverse(str2) << std::endl;

    // vector 뒤집기
    std::vector<int> vec1{ 10, 20, 30, 40, 50 };
    std::vector<int> vec2 = reverseVector<int>(vec1);

    for (auto e : vec2)
    {
        std::cout << e << ",";
    }
    std::cout << std::endl;
}
*/

/*
// 제공받은 stack으로 예제 풀이
int main()
{
    std::stack<int> stk;

    stk.push(10);
    stk.push(20);
    stk.push(30);

    std::cout << stk.top() << std::endl;

    stk.pop();
    std::cout << stk.top() << std::endl;

    stk.push(40);
    std::cout << stk.size() << std::endl;
    while (!stk.empty())
    {
        auto e = stk.top();
        std::cout << e << ",";
        stk.pop();
    }
    std::cout << std::endl;
}
*/

/*
int main()
{
    // 구현한 stack으로 예제 풀이
    Stack_Vector<int> vs;

    vs.push(10);
    vs.push(20);
    vs.push(30);

    std::cout << vs.top() << std::endl;

    vs.pop();
    std::cout << vs.top() << std::endl;

    vs.push(40);
    std::cout << vs.size() << std::endl;
    while (!vs.empty())
    {
        auto e = vs.top();
        std::cout << e << ",";
        vs.pop();
    }
    std::cout << std::endl;



    Stack_Forward<int> ls;

    ls.push(10);
    ls.push(20);
    ls.push(30);

    std::cout << ls.top() << std::endl;

    ls.pop();
    std::cout << ls.top() << std::endl;

    ls.push(40);
    std::cout << ls.size() << std::endl;
    while (!ls.empty())
    {
        auto e = ls.top();
        std::cout << e << ",";
        ls.pop();
    }
    std::cout << std::endl;
}
*/